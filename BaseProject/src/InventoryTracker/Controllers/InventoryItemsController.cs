using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using InventoryTracker.Models;
using Sakura.AspNetCore;
using System.Data;
using Microsoft.AspNetCore.Authorization;

namespace InventoryTracker.Controllers
{
    public class InventoryItemsController : Controller
    {
        private readonly DBContexts _context;

        public object pageNumber { get; private set; }

        public InventoryItemsController(DBContexts context)
        {
            _context = context;    
        }

        // GET: InventoryItems
        public IActionResult Index(string sortOrder)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "Item_Name_desc" : "";
            ViewBag.QuantitySortParm = sortOrder == "Quantity" ? "Quantity_desc" : "Quantity";
            ViewBag.UserSortParm = sortOrder == "User" ? "User_desc" : "User";
           
            var order = from s in _context.InventoryItems
                        select s;
            
            switch (sortOrder)
            {
                case "Item_Name_desc":
                    order = order.OrderByDescending(s => s.Name);
                    break;
                case "Quantity_desc":
                    order = order.OrderByDescending(s => s.Quantity);
                    break;
                case "Quantity":
                    order = order.OrderBy(s => s.Quantity);
                    break;
                case "User":
                    order = order.OrderBy(s => s.CreatedByUserID);
                    break;
                case "User_desc":
                    order =  order.OrderByDescending(s => s.CreatedByUserID);
                    break;
                default:
                    order = order.OrderBy(s => s.Name);
                    break;
            }
            return View(order.ToList());
        }

        // GET: InventoryItems/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var inventoryItem = await _context.InventoryItems
                .SingleOrDefaultAsync(m => m.ID == id);
            if (inventoryItem == null)
            {
                return NotFound();
            }

            return View(inventoryItem);
        }

        // GET: InventoryItems/Create
        [Authorize]
        public IActionResult Create()
        {
            return View();
        }

        // POST: InventoryItems/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Create([Bind("ID,CreatedByUserID,Name,Quantity")] InventoryItem inventoryItem)
        {
            if (ModelState.IsValid)
            {
                
                {
                    _context.Add(inventoryItem);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
             }
            return View(inventoryItem);
        }

        // GET: InventoryItems/Edit/5
        [Authorize(Policy="Dickbuttz")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var inventoryItem = await _context.InventoryItems.SingleOrDefaultAsync(m => m.ID == id);
            if (inventoryItem == null)
            {
                return NotFound();
            }
            return View(inventoryItem);
        }

        // POST: InventoryItems/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Policy = "Dickbuttz")]
        public async Task<IActionResult> Edit(int id, [Bind("ID,CreatedByUserID,Name,Quantity")] InventoryItem inventoryItem)
        {
            if (id != inventoryItem.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(inventoryItem);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!InventoryItemExists(inventoryItem.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(inventoryItem);
        }

        // GET: InventoryItems/Delete/5
        [Authorize(Policy = "Dickbuttz")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var inventoryItem = await _context.InventoryItems
                .SingleOrDefaultAsync(m => m.ID == id);
            if (inventoryItem == null)
            {
                return NotFound();
            }

            return View(inventoryItem);
        }

        // POST: InventoryItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Policy = "Dickbuttz")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var inventoryItem = await _context.InventoryItems.SingleOrDefaultAsync(m => m.ID == id);
            _context.InventoryItems.Remove(inventoryItem);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool InventoryItemExists(int id)
        {
            return _context.InventoryItems.Any(e => e.ID == id);
        }
    }
}
