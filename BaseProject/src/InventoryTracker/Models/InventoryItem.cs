﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace InventoryTracker.Models
{
    public class InventoryItem
    {
        public int ID { get; set; }
        public int CreatedByUserID { get; set; }
        public String Name { get; set; }
        public int Quantity { get; set; }
    }

}
