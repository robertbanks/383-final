﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using InventoryTracker.Models;

namespace InventoryTracker.Migrations
{
    [DbContext(typeof(DBContexts))]
    [Migration("20170202015552_MySecondMigration")]
    partial class MySecondMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.0-rtm-22752")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("InventoryTracker.Models.InventoryItem", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CreatedByUserID");

                    b.Property<string>("Name");

                    b.Property<int>("Quantity");

                    b.HasKey("ID")
                        .HasName("UniqueKey_InventoryID");

                    b.HasIndex("CreatedByUserID");

                    b.ToTable("InventoryItems");
                });

            modelBuilder.Entity("InventoryTracker.Models.User", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("FirstName");

                    b.Property<string>("LastName");

                    b.Property<string>("Password");

                    b.Property<string>("Username")
                        .IsRequired();

                    b.HasKey("ID")
                        .HasName("UniqueKey_UserID");

                    b.HasAlternateKey("Username")
                        .HasName("UniqueKey_Username");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("InventoryTracker.Models.InventoryItem", b =>
                {
                    b.HasOne("InventoryTracker.Models.User")
                        .WithMany("InventoryItems")
                        .HasForeignKey("CreatedByUserID")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
