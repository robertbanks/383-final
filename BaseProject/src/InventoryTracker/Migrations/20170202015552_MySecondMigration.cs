﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace InventoryTracker.Migrations
{
    public partial class MySecondMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Users",
                table: "Users");

            migrationBuilder.DropPrimaryKey(
                name: "PK_InventoryItems",
                table: "InventoryItems");

            migrationBuilder.AlterColumn<string>(
                name: "Username",
                table: "Users",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "UniqueKey_UserID",
                table: "Users",
                column: "ID");

            migrationBuilder.AddUniqueConstraint(
                name: "UniqueKey_Username",
                table: "Users",
                column: "Username");

            migrationBuilder.AddPrimaryKey(
                name: "UniqueKey_InventoryID",
                table: "InventoryItems",
                column: "ID");

            migrationBuilder.CreateIndex(
                name: "IX_InventoryItems_CreatedByUserID",
                table: "InventoryItems",
                column: "CreatedByUserID");

            migrationBuilder.AddForeignKey(
                name: "FK_InventoryItems_Users_CreatedByUserID",
                table: "InventoryItems",
                column: "CreatedByUserID",
                principalTable: "Users",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_InventoryItems_Users_CreatedByUserID",
                table: "InventoryItems");

            migrationBuilder.DropPrimaryKey(
                name: "UniqueKey_UserID",
                table: "Users");

            migrationBuilder.DropUniqueConstraint(
                name: "UniqueKey_Username",
                table: "Users");

            migrationBuilder.DropPrimaryKey(
                name: "UniqueKey_InventoryID",
                table: "InventoryItems");

            migrationBuilder.DropIndex(
                name: "IX_InventoryItems_CreatedByUserID",
                table: "InventoryItems");

            migrationBuilder.AlterColumn<string>(
                name: "Username",
                table: "Users",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddPrimaryKey(
                name: "PK_Users",
                table: "Users",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_InventoryItems",
                table: "InventoryItems",
                column: "ID");
        }
    }
}
