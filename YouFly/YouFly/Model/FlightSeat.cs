﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YouFly.Model
{
    public class FlightSeat
    {
        public int FlightSeatId { get; set; }
        public string SeatLocation { get; set; }
        public string SeatNumber { get; set; }
        public string SeatCapacity { get; set; }
        public string SeatType { get; set; }
        public  decimal Pricing { get; set; }
        public int FlightRouteId { get; set; }

        public virtual FlightRoute FlightRoute { get; set; }
    }
}
