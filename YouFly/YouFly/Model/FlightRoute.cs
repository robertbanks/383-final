﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YouFly.Model
{
    public class FlightRoute
    {
        //public FlightRoute()
        //{
        //    this.Airport = new HashSet<Airport>();
        //}

        public int FlightRouteID { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public DateTime DepartureDateTime { get; set; }
        public DateTime ArrivalDateTime { get; set; }
        public string status { get; set; }
        public string Comment { get; set; }
        public DateTime LastStatusDateTime { get; set; }
        public int AirportID { get; set; }

        public virtual Airport Airport { get; set; }
        public virtual ICollection<Flight> Flights { get; set; }
        public virtual ICollection<FlightSeat> FlightSeats { get; set; }
    }
}