﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YouFly.Model
{
    public class User
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int PhoneNumber { get; set; }
        public string Email { get; set; }
        public bool IsAdmin { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        public int CreditCardNumber { get; set; }
        public DateTime CCExpiration { get; set; }
        public string CCType { get; set; }
        public string Comment { get; set; }
        public string Status { get; set; }

        public virtual ICollection<Flight> Flights { get; set; }


    }
}
