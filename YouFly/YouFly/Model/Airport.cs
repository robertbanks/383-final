﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YouFly.Model
{
    public class Airport
    {

        public  int AirportID { get;set;}
        public  string AirportCode { get;set;}
        public  string City { get;set;}
        public  string State { get;set;}
        public  string Country { get;set;}

        public virtual ICollection<Airport> Airports { get; set; }

    }
}
