﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YouFly.Model
{
    public class Flight
    {
        public int FlightID{ get; set; }
        public string Comments { get; set; }

        public int UserID { get; set; }
        public int FlightRouteID  { get; set; }
        public int FlightSeatID { get; set; }

        public virtual FlightRoute FlightRoute { get; set; }
        public virtual User User { get; set; }
        public virtual FlightSeat FlightSeat { get; set; }

    }
}
