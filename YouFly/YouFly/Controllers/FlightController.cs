﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using YouFly.Data;
using YouFly.Model;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace YouFly.Controllers
{
    [Route("flight/[controller]")]
    public class FlightController : Controller
    {
        private readonly YouFlyDBContext _context;

        public FlightController(YouFlyDBContext context)
        {
            _context = context;
        }

        [Route("/")]
        [HttpPost]
        public async Task<IActionResult> GetFlight()
        {
            var listOfFlightRoutes = _context.FlightRoutes.Take(300);

            return Ok(listOfFlightRoutes);
        }
    }
}