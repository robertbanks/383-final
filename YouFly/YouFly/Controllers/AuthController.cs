﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using YouFly.Data;
using YouFly.Model;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace YouFly.Controllers
{
    [Route("auth/[controller]")]
    public class AuthController : Controller
    {
        private readonly YouFlyDBContext _context;

        public AuthController(YouFlyDBContext context)
        {
            _context = context;
        }

        [Route("/login")]
        [HttpPost]
        public async Task<IActionResult> login(string email, string password)
        {
            //Handle the password hash
            var user = await _context.Users
                .Where(x => x.Email == email && x.Password == password).SingleOrDefaultAsync();

            if (user == null)
            {
                return NotFound();
            }
            // More Safe way
            var response = new User()
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                IsAdmin = user.IsAdmin
            };
            return Ok(response);
        }

        [Route("/register")]
        [HttpPost]
        public async Task<IActionResult> register(string userString)
        {
            User newUser = JsonConvert.DeserializeObject<User>(userString);

            _context.Users.Add(newUser);
            _context.SaveChanges(); // Data validation should be done in the front end
            return Ok(); //TODO: Implement more
        }
    }
}