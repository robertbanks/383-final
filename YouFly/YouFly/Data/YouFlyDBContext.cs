﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using YouFly.Model;

namespace YouFly.Data
{
    public class YouFlyDBContext : DbContext
    {

        public DbSet<Airport> Airports { get; set; }

        public DbSet<FlightRoute> FlightRoutes { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<Flight> Flights { get; set; }

        public DbSet<FlightSeat> FlightSeats { get; set; }

}
}
