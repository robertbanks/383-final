import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: []
})
export class LoginComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  submitLogin(username : string, password : string){
    console.log(username);
    console.log(password);

    //TODO Add all the things for logging things

    //Redirect to some page
    this.router.navigate(['/user']);
  }
}
