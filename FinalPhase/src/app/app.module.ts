import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { FlightsComponent } from './flights/flights.component';
import { CreateAccountComponent } from './create-account/create-account.component';
import { UserComponent } from './user/user.component';

const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  {path: 'flights', component: FlightsComponent},
  {path: 'create', component: CreateAccountComponent},
  {path: 'user', component: UserComponent},
  { path: '',   redirectTo: '/login', pathMatch: 'full' }//,
  //{ path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    FlightsComponent,
    CreateAccountComponent,
    UserComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
