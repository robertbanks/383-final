import { FinalPhasePage } from './app.po';

describe('final-phase App', () => {
  let page: FinalPhasePage;

  beforeEach(() => {
    page = new FinalPhasePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
